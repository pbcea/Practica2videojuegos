﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        bool isWalking = animator.GetBool("isWalking");
        bool isRunnin = animator.GetBool("isRunnin");
        bool isJumping = animator.GetBool("isJumping");
        bool isFastJumping = animator.GetBool("isFastJumping");

        bool walkingPressed = Input.GetKey("w");
        bool runninPressed = Input.GetKey(KeyCode.LeftShift);
        bool jumpingPressed = Input.GetKey(KeyCode.Space);
        bool JumpingFastPressed = Input.GetKey(KeyCode.Space);

        if(!isWalking && walkingPressed){
            Debug.Log("Paso 1");
            animator.SetBool("isWalking", true);
        }

        if(isWalking && !walkingPressed){
            Debug.Log("Paso 2");
            animator.SetBool("isWalking", false);
        }

        if(!isRunnin && (runninPressed && walkingPressed)){
            Debug.Log("Paso 3");
            animator.SetBool("isRunnin", true);
        }

        if(isRunnin && (!walkingPressed || !runninPressed)){
            Debug.Log("Paso 4");
            animator.SetBool("isRunnin", false);
        }

        if(!isWalking && !isRunnin && jumpingPressed){
            Debug.Log("Paso 5");
            animator.SetBool("isJumping", true);
        }

        if(!isWalking && !isRunnin && !jumpingPressed){
            Debug.Log("Paso 6");
            animator.SetBool("isJumping", false);
        }
        
        if(isWalking && jumpingPressed){
            Debug.Log("Paso 7");
            animator.SetBool("isJumping", true);
        }

        if(isWalking && !jumpingPressed){
            Debug.Log("Paso 8");
            animator.SetBool("isJumping", false);
        }

        if(isRunnin && JumpingFastPressed){
            Debug.Log("Paso 9");
            animator.SetBool("isFastJumping", true);
        }

        if(isRunnin && !JumpingFastPressed){
            Debug.Log("Paso 10");
            animator.SetBool("isFastJumping", false);
        }
    }
}
